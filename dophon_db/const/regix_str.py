# coding:utf-8
get_mapper_file_name = '\\..*'
get_at_least_one_blank = '\\s+'
mapper_bound_field_check = '(\#|\$|\!|\@|\?)\{.*\}'
select_sql_check = '^\\s*(s|S)(e|E)(l|L)(e|E)(c|C)(t|T)\\s+.+'
